# VFP9 Compile Tool
VFP9 Compile Tool is a program that compiles PRG files of a directory, compiles them and, if there are any errors (checking if '< filename >.ERR exists), show them in a browse window of a cursor inside Fox Pro.

### Current Functionality
* Compile programs of current working directory of vfp9.

### Future Functionality
* Compile only one PRG file.
* Compile given folder.
* Compile with recursive method folder and its subfolders.
