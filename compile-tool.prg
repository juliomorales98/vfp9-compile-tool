#DEFINE ceNoExists "File doesnt exists"
#DEFINE ceSucces "Compiled Succesfully"
#DEFINE ceError "Error while compiling"
#DEFINE ceErrFile "Cannot work with file"
#DEFINE cePrg "Microsoft Visual FoxPro Program"

*Cursor for showng responses of compiles
CREATE CURSOR cCmpErrors( id c(20), errors c(100))
INDEX ON id TAG id

CREATE CURSOR cCmpLog( id c(20), file c(100), path c(200), status c(25) )
INDEX ON id TAG id
SET RELATION TO id INTO cCmpErrors

* Parse through parameters sent
*If error in parameter exit
DO CASE
    CASE PARAM() = 0 && No params, compile all prg in current dir
        compileCurDir()
ENDCASE

SELECT cCmpLog
GO TOP
BROWSE NOWAIT

IF RECCOUNT( "cCmpErrors" ) > 0
  SELECT cCmpErrors
  GO TOP
  BROWSE NOWAIT
ENDIF

RETURN

* Function to compile in the current directory
FUNCTION compileCurDir()
    lcCurDir = ".\"
    loFs = CreateObject( "Scripting.FileSystemObject" )
    loFolder = loFs.GetFolder(lcCurDir)
    loFiles = loFolder.Files
    FOR EACH fi IN loFiles
        IF fi.type = cePrg
          lcId = ALLTRIM(SYS(3))
          llSucces = mainCompile( fi.name, ADDBS(fi.parentFolder.path), lcId)
          INSERT INTO cCmpLog ;
            VALUES( lcId, fi.name, fi.path, IIF( llSucces, ceSucces, ceError))
        ENDIF
    ENDFOR
    RETURN
ENDFUNC

* Main compile function
FUNCTION mainCompile( lcFile, lcPath, lcId) 
    *Check if prg exist, if not exists next
    IF !FILE( lcFile )
        INSERT INTO cCmpErrors VALUES( lcId, ceNoExists)
        RETURN .F.
    ENDIF
    * compile file
    LOCAL lcpathFile, lcErrFile, llSucces
    CREATE CURSOR cErrTmp( line c(200))
    lcPathFile = lcpath + lcfile && function to add diagonal
    lcErrFile = STRTRAN(LOWER(lcfile), ".prg", ".ERR")
    llSucces = .T.

    TRY
      compile (lcPathFile)
    CATCH
      llSucces = .F.
      INSERT INTO cCmpErrors VALUES( lcid, ceErrFile )
    ENDTRY

    *check for .err files
    * if exists, append lines to single line in cursor
    IF FILE( lcErrFile) 
        SELECT cErrTmp
        APPEND FROM (lcErrFile) TYPE DELIMITED WITH ""
        SCAN
            INSERT INTO cCmpErrors VALUES( lcid, cErrTmp.line)
        ENDSCAN
        llSucces = .F.
    ENDIF

    USE IN cErrTmp
    RETURN llSucces
ENDFUNC

